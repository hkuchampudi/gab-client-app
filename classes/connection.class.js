/* eslint-disable no-underscore-dangle */
/* eslint-disable class-methods-use-this */
// Load necessary NPM modules
const WebSocket = require('ws');

// Load local module dependencies
const Logger = require('../modules/winston.logger');
const ConnectionEmitter = require('../modules/emitters/connection.emitter');
const ServerMessageParser = require('../modules/parsers/serverMessage.parser');

/**
 * Connection Class that manages the client's connection to the
 * GAB server, and provides useful abstractions for sending
 * messages to the GAB Server.
 * @access public
 *
 * @example
 * const Connection = new GABServerConnection();
 * Connection.setupConnection('localhost', '4930', 'Harsha');
 */
class Connection {
  /**
   * Setup a WS connection to the GAB Server located
   * at the provided hostname and port. The user will
   * identify with the GAB Server by the provided username.
   * @access public
   *
   * @listens WebSocket.open      GAB Server connection has opened
   * @listens WebSocket.close     GAB Server connection has closed
   * @listens WebSocket.error     GAB Server connection error message
   * @listens WebSocket.message   GAB Server connection message
   *
   * @param {string} hostname - Hostname of the GAB Server
   * @param {number} port     - Port that the GAB Server is listening on
   * @param {string} username - Username to identify as
   * @param {function} callback - The callback function to pass the created socket and username to
   */
  setupConnection(hostname, port, username, callback) {
    // Log the host connection information
    Logger.info(`connecting to the GAB Server: ${hostname}, ${port}, ${username}`);
    // Set the connection information as private
    // class variables so they can be used throughout
    this._hostname = hostname;
    this._port = port;
    this._username = username;
    // Attempt to create a WS connection to the
    // GAB Server located at the specified address
    this._socket = new WebSocket(`ws://${this._hostname}:${this._port}/?username=${this._username}`);
    // Setup listeners for events emitted by the socket
    this._socket.on('open', this.__connectionOpenedHandler.bind(this));
    this._socket.on('close', this.__connectionClosedHandler.bind(this));
    this._socket.on('error', this.__connectionErrorHandler.bind(this));
    this._socket.on('message', this.__connectionMessageHandler.bind(this));
    // Execute the callback function, and pass in the created socket
    callback(this._socket, this._username);
  }

  /**
   * Adds a socket public property that returns
   * the connection socket
   * @
   */
  get socket() {
    return this._socket;
  }

  /**
   * Handler function for when the client opens a connection
   * with the GAB Server. It then notifies the Connection UI
   * to hide, and the Chat UI to load.
   * @access private
   *
   * @emits ConnectionEmitter.success   A successful connection was made
   */
  __connectionOpenedHandler() {
    ConnectionEmitter.emit('success');
  }

  /**
   * Handler function for when the client's connection with
   * the GAB Server is closed. It notifies the Connection UI
   * that it must be displayed, and that the Chat UI should
   * be hidden.
   * @access private
   *
   * @emits ConnectionEmitter.failure
   */
  __connectionClosedHandler() {
    ConnectionEmitter.emit('failure', 'connection to GAB Server was closed');
    // Delete the socket and remove all listeners tied
    // to that socket so, when a new socket connection
    // is made, duplicate events will not occur
    this._socket.removeAllListeners();
    delete this._socket;
  }

  /**
   * Handler function for when an error occurs with the client's
   * connection to the GAB Server. It notifies the Connection UI
   * that it should be displayed, and that the Chat UI should be
   * hidden.
   * @access private
   *
   * @emits ConnectionEmitter.failure
   *
   * @param {object} - An Error object detailing the cause of the error
   */
  __connectionErrorHandler(error) {
    // Log the error message
    Logger.error(error.message);
    ConnectionEmitter.emit('failure', error.message);
    // Delete the socket and remove all listeners tied
    // to that socket so, when a new socket connection
    // is made, duplicate events will not occur
    this._socket.removeAllListeners();
    delete this._socket;
  }

  /**
   * Handler function for when a message is received from the
   * GAB Server.
   * @access private
   *
   * @param {object} - A message object sent by the GAB server
   */
  __connectionMessageHandler(message) {
    ServerMessageParser.bind(this)(message);
  }

  /**
   * Helper function that sends a message to the GAB server,
   * requesting an updated list of online users.
   * @access public
   */
  updateUserlist() {
    this._socket.send(JSON.stringify({
      from: this._username,
      to: 'undefined',
      kind: 'userlist',
      data: 'undefined',
    }));
  }

  /**
   * Helper function that sends a whoami message to the GAB
   * Server, so the current user can be identified.
   * @access public
   */
  sendWhoAmI() {
    this._socket.send(JSON.stringify({
      from: this._username,
      to: 'undefined',
      kind: 'whoami',
      data: 'undefined',
    }));
  }

  /**
   * Helper function that sends a direct message to the specified
   * user with the specified data payload.
   * @access public
   *
   * @param {string} recipient - The user to send the direct message to
   * @param {string / object} data - The data payload of the direct message
   */
  sendDirectMessage(recipient, data) {
    this._socket.send(JSON.stringify({
      from: this._username,
      to: recipient,
      kind: 'direct',
      data,
    }));
  }

  /**
   * Helper function that sends a generic chat message to all
   * online users connected to the GAB Server. The contents of
   * the message is specified by the data parameter.
   * @access public
   *
   * @param {string} data - Chat message
   */
  sendChatMessage(data) {
    this._socket.send(JSON.stringify({
      from: this._username,
      to: 'all',
      kind: 'chat',
      data,
    }));
  }
}

// Create an instance of the Connection class that will be
// shared across all modules that import it.
const GABServerConnection = new Connection();

// Export the created instance of the Connection class
module.exports = GABServerConnection;
