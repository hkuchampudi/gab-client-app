/* eslint-disable no-underscore-dangle */
// Load required NPM modules
const FileSystem = require('fs');
const Assert = require('chai').assert;

// Load local file dependencies
const Logger = require('../modules/winston.logger');

// Regex
const REGEX_HEX_COLOR_CODE = /^#(?:[0-9a-fA-F]{3}){1,2}$/;

class StyleManager {
  /**
   * Constructor function that loads style.json from the root
   * application directory and validates it. If it is valid, the
   * styles specified will be used; otherwise, no styles will be
   * applied to the messages. Any errors will be logged to the
   * log file.
   */
  constructor() {
    // Check if the file exists, and if it is readable
    this._styleFilename = 'style.json';
    this._styleConfig = [];
    this._validAttributes = ['bold', 'underline', 'blink', 'inverse', 'invisible'];
    try {
      if (!FileSystem.lstatSync(this._styleFilename).isFile()) {
        throw new Error(`${this._styleFilename}: ${this._styleFilename} is not a file`);
      }
      FileSystem.accessSync(this._styleFilename, FileSystem.constants.R_OK);
      const styleGuide = FileSystem.readFileSync(this._styleFilename);
      const parsedStyleGuide = JSON.parse(styleGuide);
      Assert.isArray(parsedStyleGuide, 'invalid configuration file');
      Assert.isAbove(parsedStyleGuide.length, 0, 'configuration is empty');
      parsedStyleGuide.forEach((element) => {
        const jsonString = JSON.stringify(element);
        const parsedEntry = {};
        Assert.isObject(element, `invalid configuration line: ${jsonString}`);
        const { pattern, type, value } = element;
        // parsedEntry.pattern = pattern.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
        const _ = new RegExp(parsedEntry.pattern, 'gm');
        Assert.include(['attribute', 'color'], type, `invalid configuration line: ${jsonString}`);
        if (type === 'attribute') Assert.include(this._validAttributes, value, `invalid configuration line: ${jsonString}`);
        if (type === 'color') Assert.match(value, REGEX_HEX_COLOR_CODE, `invalid configuration line: ${jsonString}`);
        parsedEntry.pattern = pattern;
        parsedEntry.type = type;
        parsedEntry.value = value;
        this._styleConfig.push(parsedEntry);
      });
      Logger.info(`successfully loaded ${this._styleFilename}`);
    } catch (error) {
      this._styleConfig = [];
      Logger.error(`${this._styleFilename}: ${error.message}`);
    }
  }

  /**
   * Applies all applicable styles to an input string based on the configuration
   * provided in style.json
   * @access public
   * @param {string} input - Input string to style
   * @returns {string} The styled message is returned
   */
  styleMessage(input) {
    if (this._styleConfig.length === 0) return input;
    let styledMessage = input;
    // Apply all matching styling for the input text
    this._styleConfig.forEach((style) => {
      const pattern = new RegExp(style.pattern, 'gm');
      if (pattern.test(styledMessage)) {
        if (style.type === 'attribute') {
          styledMessage = styledMessage.replace(pattern, `{${style.value}}$&{/${style.value}}`);
        } else if (style.type === 'color') {
          styledMessage = styledMessage.replace(pattern, `{${style.value}-fg}$&{/${style.value}-fg}`);
        }
      }
    });
    // Return the styled message
    return styledMessage;
  }
}

// Create an instance of the StyleManager that will
// be shared across all the modules that import it
const SManager = new StyleManager();

// Export the created instance of the class
module.exports = SManager;
