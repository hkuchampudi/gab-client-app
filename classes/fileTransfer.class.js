/* eslint-disable no-underscore-dangle */
/* eslint-disable class-methods-use-this */
// Load necessary NPM modules
const Crypto = require('crypto');
const FileSystem = require('fs');
const Path = require('path');

// Load local module dependencies
const Logger = require('../modules/winston.logger');

// Define constants
// const FILE_STREAM_CHUNK_SIZE = 10000 * 1024;
const FILE_CHUNK_SIZE_BYTES = 1000000;
// const FILE_STREAM_CHUNK_SIZE = 100000 * 1024;

/**
 * File Transfer Manager Class that manages file
 * transfer operations between GAB Clients, and
 * provides useful abstractions for those operations.
 * @access public
 *
 * @example
 * const FTManager = new FileTransferManager();
 */
class FileTransferManager {
  /**
   * Constructor function that initializes the incoming
   * and outgoing file transfer requests.
   */
  constructor() {
    this._incomingRequests = {};
    this._outgoingRequests = {};
  }

  /**
   * Sets the socket and username which can be used to
   * communicate with the remote GAB server.
   * @access public
   * @param {WebSocket} socket - A Connection class socket
   * @param {string} username - The username of the connected user
   */
  setConnection(socket, username) {
    this._socket = socket;
    this._username = username;
  }

  sendDirectMessage(recipient, data) {
    this._socket.send(JSON.stringify({
      from: this._username,
      to: recipient,
      kind: 'direct',
      data,
    }));
  }

  /**
   * When a file transfer request is received by the client, it will
   * be added to the lookup table that stores all the incoming requests.
   * @access public
   *
   * @param {string} sender   - Username of user that sent the request
   * @param {string} filename - Name of the file to be transferred
   * @param {string} fileSize - The size of the incoming file
   * @param {string} uuid     - The unique ID associated with the transfer
   * @param {function} callback = Callback function to pass results into
   */
  addIncomingRequest(sender, filename, fileSize, uuid, callback) {
    // Check if the uuid already exists in the incomingRequests
    // table. If it already exists in the table, ignore the
    // incoming request
    if (this._incomingRequests[uuid]) {
      Logger.info(`duplicate incoming file transfer request: ${sender} ${filename} ${fileSize} ${uuid}`);
      callback({
        success: false,
        message: null,
      });
    } else {
      // Otherwise, the information can be added to the
      // incomingRequests table without a problem
      this._incomingRequests[uuid] = {
        sender,
        filename,
        file_size: fileSize,
        accepted: false,
      };
      // Execute the callback
      callback({
        success: true,
        message: this._incomingRequests[uuid],
      });
    }
  }

  /**
   * Removes an incoming file transfer request from the table of incoming
   * requests.
   * @access private
   *
   * @param {string} uuid - The unique ID associated with the request to remove
   */
  __removeIncomingRequest(uuid) {
    delete this._incomingRequests[uuid];
  }

  /**
   * Generates a random UUID by hashing the current unix time.
   * @access private
   * @returns {string} A UUID in hex format
   */
  __UUIDGenerator() {
    const Hash = Crypto.createHash('sha256');
    const EpochTime = (new Date()).getTime().toString();
    Hash.update(EpochTime);
    return Hash.digest('hex');
  }

  /**
   * Creates an outgoing file transfer request, and then
   * sends the target user the transfer request.
   * @access public
   *
   * @param {string} recipient  - Username of the destination user
   * @param {string} filePath   - System path where the file to transfer is
   * @return {object} JSON object with 'success' (boolean) and 'message' (object) keys
   */
  addOutgoingRequest(recipient, filePath) {
    try {
      // Validate whether the filePath provided is a valid path
      // to a file on the system and that the file is readable
      // by the GAB client
      FileSystem.accessSync(filePath, FileSystem.constants.R_OK);
      // Make sure that the path points to a file
      if (!FileSystem.lstatSync(filePath).isFile()) throw new Error('file path must point to a file');
      // Get the filename from the filePath
      const filename = Path.basename(filePath);
      // Get the size of the file to be transferred
      const fileSize = FileSystem.statSync(filePath).size;
      // Generate a unique ID (uuid) for the file transfer request
      const uuid = this.__UUIDGenerator();
      // Add the outgoing request to the outgoingRequests table
      this._outgoingRequests[uuid] = {
        recipient,
        filename,
        file_size: fileSize,
        file_path: filePath,
        bytes_transferred: 0,
      };
      // Create the encoded File Transfer Request, and send it as
      // a direct message to the recipient.
      const data = `FileTransferRequest::${filename}::${fileSize}::${uuid}`;
      this.sendDirectMessage(recipient, data);
      // Return that the file transfer request was sent
      return {
        success: true,
        message: this._outgoingRequests[uuid],
      };
    } catch (error) {
      // Log the error message
      Logger.warn(error.message);
      return {
        success: false,
        message: error.message,
      };
    }
  }

  /**
   * Removes an outgoing file transfer request from the table of outgoing
   * requests.
   * @access private
   *
   * @param {string} uuid - The unique ID associated with the request to remove
   */
  __removeOutgoingRequest(uuid) {
    delete this._outgoingRequests[uuid];
  }

  /**
   * Accepts a request (specified by uuid) that was sent to the user. The
   * incoming request will be marked as accepted in the incomingRequests
   * table. Finally, the client will send an accept message to the original
   * sending user
   * @access public
   *
   * @param {string} uuid - The unique ID associated with the request to accept
   * @return {object} JSON object with 'success' (boolean) and 'message' (object) keys
   */
  acceptRequest(uuid) {
    // Make sure that an incomingRequest with the input uuid
    // exists. Then, mark the transfer as accepted and send an
    // encoded acceptance message to the sending user.
    if (this._incomingRequests[uuid]) {
      this._incomingRequests[uuid].accepted = true;
      const { sender } = this._incomingRequests[uuid];
      const data = `FileTransferAccepted::${uuid}`;
      this.sendDirectMessage(sender, data);
      return {
        success: true,
        message: this._incomingRequests[uuid],
      };
    }
    // Otherwise, if a matching uuid was not found in the table,
    // return an error message
    Logger.warn(`no matching uuid found to accept: ${uuid}`);
    return {
      success: false,
      message: 'no matching uuid found to accept',
    };
  }

  /**
   * Rejects a request (specified by uuid) that was sent to the user. The
   * incoming request will be marked as rejected in the incomingRequests
   * table. Finally, the client will send an reject message to the original
   * sending user
   * @access public
   *
   * @param {string} uuid - The unique ID associated with the request to reject
   * @return {object} JSON object with 'success' (boolean) and 'message' (object) keys
   */
  rejectRequest(uuid) {
    // Make sure that an incomingRequest with the input uuid
    // exists. Then, delete the request from the table and send
    // an encoded rejection message to the sending user.
    if (this._incomingRequests[uuid]) {
      const { sender } = this._incomingRequests[uuid];
      this.__removeIncomingRequest(uuid);
      const data = `FileTransferRejected::${uuid}`;
      this.sendDirectMessage(sender, data);
      return {
        success: true,
        message: null,
      };
    }
    // Otherwise, if a matching uuid was not found in the table,
    // return an error message
    Logger.warn(`no matching uuid found to accept: ${uuid}`);
    return {
      success: false,
      message: 'no matching uuid found to accept',
    };
  }

  /**
   * Helper function to read specific bytes in a file.
   * @access private
   *
   * @param {string} filePath - The path of the file to read
   * @param {int} offset - The byte offset for where to start reading the file
   * @param {int} size - The number of bytes to read in the file
   * @param {function} callback - The function to pass the read data to: (error, bytesRead, buffer)
   */
  __readFileChunk(filePath, offset, size, callback) {
    FileSystem.open(filePath, 'r', (openError, fd) => {
      if (openError) callback(openError, null, null);
      else {
        FileSystem.read(fd, Buffer.alloc(size), 0, size, offset, (readError, bytesRead, buffer) => {
          callback(readError, bytesRead, buffer);
        });
      }
    });
  }

  /**
   * Send a user the next chunk of the file.
   * @access public
   *
   * @param {string} uuid - UUID associated with the file transfer
   * @param {function} callback - Function to pass the results of sending the next chunk to
   */
  sendNextChunk(uuid, callback) {
    try {
      // CHeck that the uuid is valid
      if (this._outgoingRequests[uuid]) {
        // Check if the file has been completely transferred
        // Otherwise, figure out the next chunk to be sent to
        // the remote GAB client
        const request = this._outgoingRequests[uuid];
        const { filename, recipient } = request;
        const remainingBytes = request.file_size - request.bytes_transferred;
        if (remainingBytes !== 0) {
          // If the remaining file size is less than the chunk size,
          // just send the rest of the file otherwise send the configured
          // file chunk size
          const bytesToRead = (remainingBytes <= FILE_CHUNK_SIZE_BYTES) ? remainingBytes : FILE_CHUNK_SIZE_BYTES;
          this.__readFileChunk(request.file_path, request.bytes_transferred, bytesToRead, (error, bytesRead, buffer) => {
            // Check if there was an error. If there was, throw an error
            // Otherwise, create a proper direct message payload to send
            // to the remote client
            if (error) throw new Error(error.message);
            else {
              this._outgoingRequests[uuid].bytes_transferred += bytesRead;
              const encodedData = buffer.toString('base64');
              const data = {
                uuid,
                filename,
                chunk: encodedData,
              };
              // Send the file chunk
              this.sendDirectMessage(recipient, data);
            }
          });
          // Call the callback function which notifies whether the
          // file is still being transferred and the number of bytes
          // remaining to be sent.
          const updatedRemainingBytes = this._outgoingRequests[uuid].file_size - this._outgoingRequests[uuid].bytes_transferred;
          const data = {
            filename,
            recipient,
            value: updatedRemainingBytes,
          };
          callback('transferring', data);
        } else {
          // When the last chunk has been transferred to the remote
          // GAB client, send a file transfer finished message
          const finishedTransferData = `FileTransferFinished::${uuid}`;
          this.sendDirectMessage(request.recipient, finishedTransferData);
          // Remove the file from the outgoingRequests
          this.__removeOutgoingRequest(uuid);
          // Call the callback function
          const data = {
            filename,
            recipient,
            value: 0,
          };
          callback('completed', data);
        }
      } else {
        throw new Error(`cannot send file: outgoing transfer ${uuid} not found`);
      }
    } catch (error) {
      // In the event of an error, log the error and then
      // notify the remote GAB client that there was an error
      // with the file transfer. Delete the file transfer from
      // the local outgoingRequests and the remote incomingRequests
      Logger.warn(error.message);
      let data = {};
      if (this._outgoingRequests[uuid]) {
        const transferDataError = `FileTransferError::${uuid}`;
        data = {
          filename: this._outgoingRequests[uuid].filename,
          recipient: this._outgoingRequests[uuid].recipient,
          value: error.message,
        };
        this.sendDirectMessage(this._outgoingRequests[uuid].recipient, transferDataError);
        this.__removeOutgoingRequest(uuid);
      }
      callback('error', data);
    }
  }

  /**
   * Notify the remote GAB client that the file transfer has started
   * @access public
   *
   * @param {string} uuid - The unique ID associated with the file to send
   * @param {function} callback - Callback function for when the file transfer message has been sent
   */
  sendFile(uuid, callback) {
    try {
      // Make sure that the uuid exists in the outgoingRequest table
      // and that the file still exists on the local machine before
      // starting the file transfer process
      if (this._outgoingRequests[uuid]) {
        const { recipient } = this._outgoingRequests[uuid];
        const filePath = this._outgoingRequests[uuid].file_path;
        FileSystem.accessSync(filePath, FileSystem.constants.R_OK);
        // Send a message to the recipient that the
        // file transfer is starting
        const startingTransferData = `FileTransferStarted::${uuid}`;
        this.sendDirectMessage(recipient, startingTransferData);
        callback(null);
      } else {
        throw new Error(`cannot send file: outgoing transfer ${uuid} not found`);
      }
    } catch (error) {
      // In the event of an error, log the error
      // call the callback function
      Logger.error(error.message);
      callback(error);
    }
  }

  /**
   * Receives the file chunks from the sending GAB client and writes the
   * chunks to file.
   * @access public
   *
   * @param {string} uuid - The unique ID associated with the file to remove
   * @param {string} chunk - The chunk of base64 encoded data that was sent.
   */
  receiveFileChunk(uuid, chunk) {
    // Make sure that the uuid exists in incomingRequests
    // and that the file transfer request has been accepted.
    if ((this._incomingRequests[uuid] || {}).accepted) {
      // Decode the received chunk and write
      // the contents to the destination file
      const { filename, sender } = this._incomingRequests[uuid];
      const data = Buffer.from(chunk, 'base64');
      FileSystem.appendFile(`files/${filename}`, data, (err) => {
        if (err) Logger.err(err.message);
        else {
          // Send a message back to the sending user that
          // the chunk was successfully received and written
          const receivedMessage = `FileTransferChunkReceived::${uuid}`;
          this.sendDirectMessage(sender, receivedMessage);
        }
      });
    } else {
      // Log that an unrequested file chunk was received
      Logger.info(`received unapproved chunk with uuid ${uuid}`);
    }
  }

  /**
   * Gets the file information stored for a uuid in the
   * incoming requests table
   * @param {string} uuid - The unique ID associated with the file desired
   * @returns {object} JSON object with 'success' (boolean) and 'message' (object) keys
   */
  incomingFileInformation(uuid) {
    if (this._incomingRequests[uuid]) {
      return {
        success: true,
        message: this._incomingRequests[uuid],
      };
    }
    return {
      success: false,
      message: {},
    };
  }
}

// Create an instance of the FileTransferManager class that
// will be shared across all modules that import it
const FTManager = new FileTransferManager();

// Export the created instance of the FileTransferManager
module.exports = FTManager;
