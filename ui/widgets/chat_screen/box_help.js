/* Name: Chat Command Help Box
 * Description: Blessed textarea component that shows some
 * helpful key commands that are available in the chat client.
 */
// Load required NPM modules
const Blessed = require('blessed');
const Screen = require('../../screen');

// Create the help box
const HelpBox = Blessed.textarea({
  parent: Screen,
  border: { type: 'line' },
  label: ' Help (/help) ',
  padding: 0,
  bottom: 0,
  right: 0,
  height: 11,
  width: '40%',
  style: { focus: { border: { fg: '#c15943' } } },
  hidden: true,
  value: `
  Generic Keys
  Key:  [Esc]    Toggle between windows
  Key: [CTRL-C]  Close application

  Online Users Keys
  Key:   [d]     Direct message to selected user
  Key:   [f]     File transfer request to selected user
  `,
});

// Export the created Blessed textarea object
module.exports = HelpBox;
