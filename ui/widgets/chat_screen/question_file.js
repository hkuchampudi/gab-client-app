/* Name: Chat File Transfer Request Approval Prompt
 * Description: Blessed question component that appears when
 * a user selects a specific file transfer request from the
 * file transfer request list.
 */
// Load required NPM modules
const Blessed = require('blessed');
const Screen = require('../../screen');

// Create the approval prompt
const FileTransferRequestQuestion = Blessed.question({
  parent: Screen,
  border: { type: 'line' },
  padding: 0,
  top: 'center',
  left: 'center',
  width: '80%',
  height: 20,
  label: ' File Transfer Request Approval ',
  keys: true,
  style: {
    focus: { border: { fg: '#c15943' } },
    selected: { bg: '#c15943' },
  },
});

// Export the created Blessed question object
module.exports = FileTransferRequestQuestion;
