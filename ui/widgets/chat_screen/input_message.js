/* Name: Chat Input Message Box
 * Description: Blessed textbox component that allows
 * users to input a message to be sent to the GAB server.
 */
// Load required NPM modules
const Blessed = require('blessed');
const Screen = require('../../screen');

// Create the message input box
const MessageInputBox = Blessed.textbox({
  parent: Screen,
  border: { type: 'line' },
  padding: 1,
  bottom: 0,
  left: 0,
  height: 5,
  width: '60%',
  keys: true,
  input: true,
  style: { focus: { border: { fg: '#c15943' } } },
  hidden: true,
  inputOnFocus: true,
});

// Export the created Blessed input object
module.exports = MessageInputBox;
