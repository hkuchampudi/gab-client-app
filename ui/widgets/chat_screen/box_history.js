/* Name: Chat History Box
 * Description: Blessed box component that displays and
 * stores the chat history.
 */
// Load required NPM modules
const Blessed = require('blessed');
const Screen = require('../../screen');

// Create the chat history box
const ChatHistoryBox = Blessed.box({
  parent: Screen,
  border: { type: 'line' },
  label: ' Chat History ',
  valign: 'bottom',
  padding: 1,
  bottom: 5,
  left: 0,
  height: 'shrink',
  width: '60%',
  keys: true,
  alwaysScroll: true,
  scrollable: true,
  scrollbar: { style: { bg: 'white' } },
  style: { focus: { border: { fg: '#c15943' } } },
  hidden: true,
  tags: true,
});

// Export the created Blessed box object
module.exports = ChatHistoryBox;
