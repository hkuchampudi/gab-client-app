/* Name: Chat Online Users List
 * Description: Blessed list component that shows a list of
 * users that are currently online. Users can be selected
 * from the list to send a direct message to.
 */
// Load required NPM modules
const Blessed = require('blessed');
const Screen = require('../../screen');

// Create the users list object
const OnlineUsersList = Blessed.list({
  parent: Screen,
  border: { type: 'line' },
  label: ' Online Users ',
  padding: 1,
  right: 0,
  bottom: 21,
  width: '40%',
  keys: true,
  alwaysScroll: true,
  scrollable: true,
  scrollbar: { style: { bg: 'white' } },
  style: {
    focus: { border: { fg: '#c15943' } },
    selected: { bg: '#c15943' },
  },
  hidden: true,
});

// Export the created Blessed list object
module.exports = OnlineUsersList;
