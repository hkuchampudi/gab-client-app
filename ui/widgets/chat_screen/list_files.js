/* Name: Chat File Transfer Intent List
 * Description: Blessed list component that shows files
 * that other users want to send to the user. The list is
 * scrollable and each request can be approved or rejected.
 */
// Load required NPM modules
const Blessed = require('blessed');
const Screen = require('../../screen');

// Create the file transfer request list
const FileTransferRequestList = Blessed.list({
  parent: Screen,
  border: { type: 'line' },
  label: ' File Transfer Requests ',
  padding: 1,
  right: 0,
  bottom: 11,
  height: 10,
  width: '40%',
  keys: true,
  alwaysScroll: true,
  scrollable: true,
  scrollbar: { style: { bg: 'white' } },
  style: {
    focus: { border: { fg: '#c15943' } },
    selected: { bg: '#c15943' },
  },
  hidden: true,
});

// Export the created list object
module.exports = FileTransferRequestList;
