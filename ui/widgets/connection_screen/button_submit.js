/* Name: Connection Form Submit Button
 * Description: Blessed button component that submits
 * the connection form and attempts to form a websocket
 * connection to the GAB server.
 */
// Load required NPM modules
const Blessed = require('blessed');
const ConnectionForm = require('./form_connection');

// Create a button object for the user
// to submit the connection information
const SubmitButton = Blessed.button({
  parent: ConnectionForm,
  name: 'submit',
  content: 'SUBMIT',
  align: 'center',
  padding: 1,
  top: 19,
  left: 5,
  width: 12,
  height: 3,
  style: {
    bold: true,
    fg: 'white',
    bg: 'green',
    focus: { inverse: true },
  },
});

// Export the created Blessed button object
module.exports = SubmitButton;
