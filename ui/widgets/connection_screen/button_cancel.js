/* Name: Connection Form Cancel Button
 * Description: Blessed button component that cancels
 * the connection form and resets all the connection
 * information in the form.
 */
// Load required NPM modules
const Blessed = require('blessed');
const ConnectionForm = require('./form_connection');

// Create a button object for the user
// to cancel the connection information
// and reset the connection form
const CancelButton = Blessed.button({
  parent: ConnectionForm,
  name: 'cancel',
  content: 'CANCEL',
  align: 'center',
  padding: 1,
  top: 19,
  left: 20,
  width: 12,
  height: 3,
  style: {
    bold: true,
    fg: 'white',
    bg: 'red',
    focus: { inverse: true },
  },
});

// Export the created Blessed button object
module.exports = CancelButton;
