/* Name: Connection Form Username Label
 * Description: Blessed text component to label the
 * username input box on the connection screen.
 */
// Load required NPM modules
const Blessed = require('blessed');
const Screen = require('../../screen');

// Create the username label
const UsernameLabel = Blessed.text({
  parent: Screen,
  top: 13,
  left: 5,
  content: 'USERNAME',
});

// Export the created Blessed label
module.exports = UsernameLabel;
