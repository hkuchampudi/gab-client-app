/* Name: Connection Form Port Label
 * Description: Blessed text component to label the
 * port input box on the connection screen.
 */
// Load required NPM modules
const Blessed = require('blessed');
const Screen = require('../../screen');

// Create the port input label
const PortLabel = Blessed.text({
  parent: Screen,
  top: 8,
  left: 5,
  content: 'PORT',
});

// Export the created Blessed port label
module.exports = PortLabel;
