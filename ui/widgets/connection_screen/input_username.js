/* Name: Connection Form Username Input
 * Description: Blessed textbox component that takes
 * in the user input for the username to be identified by
 * on the GAB server.
 */
// Load required NPM modules
const Blessed = require('blessed');
const ConnectionForm = require('./form_connection');
const Constants = require('../../../modules/constants');

// Create the username textbox input
const UsernameInput = Blessed.textbox({
  parent: ConnectionForm,
  name: 'username',
  top: 14,
  left: 5,
  height: 3,
  value: Constants.UI_USERNAME_DEFAULT_VALUE,
  border: { type: 'line' },
  style: { focus: { border: { fg: 'green' } } },
  inputOnFocus: true,
});

// Export the created Blessed input object
module.exports = UsernameInput;
