/* Name: Connection Form Hostname Input
 * Description: Blessed textbox component that takes
 * in the user input for the hostname/IP address of
 * the GAB server to connect to on the connection screen.
 */
// Load required NPM modules
const Blessed = require('blessed');
const ConnectionForm = require('./form_connection');
const Constants = require('../../../modules/constants');

// Create the hostname textbox input
const HostnameInput = Blessed.textbox({
  parent: ConnectionForm,
  name: 'hostname',
  top: 4,
  left: 5,
  height: 3,
  value: Constants.UI_HOSTNAME_DEFAULT_VALUE,
  border: { type: 'line' },
  style: { focus: { border: { fg: 'green' } } },
  inputOnFocus: true,
});

// Export the created Blessed input object
module.exports = HostnameInput;
