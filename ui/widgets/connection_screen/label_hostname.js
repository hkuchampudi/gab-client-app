/* Name: Connection Form Hostname Label
 * Description: Blessed text component to label the
 * hostname input box on the connection screen.
 */
// Load required NPM modules
const Blessed = require('blessed');
const Screen = require('../../screen');

// Create the text label for the hostname
const HostnameLabel = Blessed.text({
  parent: Screen,
  top: 3,
  left: 5,
  content: 'HOSTNAME/IP',
});

// Export the created Blessed text label
module.exports = HostnameLabel;
