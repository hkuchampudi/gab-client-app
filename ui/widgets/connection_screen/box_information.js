/* Name: Connection Information Box
 * Description: Blessed textarea component that displays
 * connection messages and errors when connecting to the
 * GAB server.
 */
// Load required NPM modules
const Blessed = require('blessed');
const Screen = require('../../screen');

// Create a textarea object to hold information about
// the existing connection
const ConnectionInformationBox = Blessed.textarea({
  parent: Screen,
  name: 'message_box',
  bottom: 0,
  width: '100%',
  height: 5,
});

// Export the created Blessed textarea object
module.exports = ConnectionInformationBox;
