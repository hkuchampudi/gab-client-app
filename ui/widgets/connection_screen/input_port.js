/* Name: Connection Form Port Input
 * Description: Blessed textbox component that takes
 * in the user input for the port number of the GAB
 * server to connect to on the connection screen.
 */
// Load required NPM modules
const Blessed = require('blessed');
const ConnectionForm = require('./form_connection');
const Constants = require('../../../modules/constants');

// Create the port textbox input
const PortInput = Blessed.textbox({
  parent: ConnectionForm,
  name: 'port',
  top: 9,
  left: 5,
  height: 3,
  value: Constants.UI_PORT_DEFAULT_VALUE,
  border: { type: 'line' },
  style: { focus: { border: { fg: 'green' } } },
  inputOnFocus: true,
});

// Export the created Blessed input object
module.exports = PortInput;
