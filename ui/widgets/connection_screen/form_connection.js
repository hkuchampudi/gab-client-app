/* Name: Connection Form
 * Description: Blessed form component that holds all
 * of the UI components where connection information
 * will be input.
 */
// Load required NPM modules
const Blessed = require('blessed');
const Screen = require('../../screen');

// Create a form object to hold the form
// input components
const ConnectionForm = Blessed.form({
  parent: Screen,
  left: 'center',
  width: '100%',
  keys: true,
});

// Export the created Blessed form object
module.exports = ConnectionForm;
