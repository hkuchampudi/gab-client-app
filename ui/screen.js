/* Name: Blessed Screen
 * Description: Blessed Screen component
 */

// Load required modules
const Blessed = require('blessed');
const Constants = require('../modules/constants');

// Create the screen object that will output the
// ncurses application components.
const Screen = Blessed.screen({
  smartCSR: true,
  title: Constants.APPLICATION_NAME,
});

// Export the created Blessed 'screen' object
module.exports = Screen;
