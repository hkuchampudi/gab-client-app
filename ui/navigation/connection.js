/* Name: Connection Screen Navigational Logic
 * Description: This file contains the implementation
 * logic for UI components on the connection screen.
 */
// Load the UI widgets that belong on the connection
// screen.
const Screen = require('../screen');
const ConnectionForm = require('../widgets/connection_screen/form_connection');
const HostnameLabel = require('../widgets/connection_screen/label_hostname');
const HostnameInput = require('../widgets/connection_screen/input_hostname');
const PortLabel = require('../widgets/connection_screen/label_port');
const PortInput = require('../widgets/connection_screen/input_port');
const UsernameLabel = require('../widgets/connection_screen/label_username');
const UsernameInput = require('../widgets/connection_screen/input_username');
const SubmitButton = require('../widgets/connection_screen/button_submit');
const CancelButton = require('../widgets/connection_screen/button_cancel');
const InformationBox = require('../widgets/connection_screen/box_information');

// Import validation modules
const HostnameValidator = require('../../modules/validators/hostname.validator');
const PortValidator = require('../../modules/validators/port.validator');
const UsernameValidator = require('../../modules/validators/username.validator');

// Import emitters
const ConnectionEmitter = require('../../modules/emitters/connection.emitter');
const ChatEmitter = require('../../modules/emitters/chat.emitter');

// Load required classes
const GABConnection = require('../../classes/connection.class');
const FileTransferManager = require('../../classes/fileTransfer.class');

// Connection Screen exit shortcuts
Screen.key(['C-c'], () => process.exit(0));
ConnectionForm.key(['C-c'], () => process.exit(0));
HostnameInput.key(['C-c'], () => process.exit(0));
PortInput.key(['C-c'], () => process.exit(0));
UsernameInput.key(['C-c'], () => process.exit(0));

/**
 * Submit Button click listener
 * @listens SubmitButton.press
 */
SubmitButton.on('press', () => {
  InformationBox.setValue('Connecting to the GAB Server...');
  Screen.render();
  ConnectionForm.submit();
});

/**
 * Cancel button click listener
 * @listens CancelButton.press
 */
CancelButton.on('press', () => {
  ConnectionForm.reset();
  Screen.render();
});

/**
 * Connection form submit event listener
 * @listens ConnectionForm.submit
 * @emits ConnectionEmitter.failure
 */
ConnectionForm.on('submit', () => {
  // Extract hostname, port, and username values from
  // the input fields
  const hostname = HostnameInput.getValue();
  const port = parseInt(PortInput.getValue(), 10) || PortInput.getValue();
  const username = UsernameInput.getValue();
  // Validate input values
  const hostnameValid = HostnameValidator(hostname);
  const portValid = PortValidator(port);
  const usernameValid = UsernameValidator(username);
  if (!hostnameValid.success) ConnectionEmitter.emit('failure', hostnameValid.message);
  else if (!portValid.success) ConnectionEmitter.emit('failure', portValid.message);
  else if (!usernameValid.success) ConnectionEmitter.emit('failure', usernameValid.message);
  else {
    // If all the validation checks pass, then clear any errors
    // that are currently being displayed and setup the connection
    // to the GAB Server and then pass the connection socket to the
    // FileTransferManager which also needs to communicate with the
    // GAB Server and the username
    InformationBox.setValue('');
    GABConnection.setupConnection(hostname, port, username, (socket, username) => {
      FileTransferManager.setConnection(socket, username);
    });
  }
});

/**
 * Listener for when the Connection Screen UI should
 * be shown to the user
 * @listens ConnectionEmitter.showUI
 */
ConnectionEmitter.on('showUI', () => {
  ConnectionForm.show();
  HostnameLabel.show();
  PortLabel.show();
  UsernameLabel.show();
  InformationBox.show();
  ConnectionForm.focus();
});

/**
 * Listener for when the Connection Screen UI should
 * be hidden to the user
 * @listens ConnectionEmitter.hideUI
 */
ConnectionEmitter.on('hideUI', () => {
  ConnectionForm.hide();
  HostnameLabel.hide();
  PortLabel.hide();
  UsernameLabel.hide();
  InformationBox.hide();
});

/**
 * When the Connection Emitter receives a 'failure' event, this signifies
 * that the Chat UI should be hidden (because the connection to the GAB
 * Server has failed) and the UI should return to the connection form with
 * a description of what caused the disconnect via the passed message.
 * @listens ConnectionEmitter.failure
 * @emits ChatEmitter.hideUI
 * @emits ConnectionEmitter.showUI
 */
ConnectionEmitter.on('failure', (message) => {
  ChatEmitter.emit('hideUI');
  ConnectionEmitter.emit('showUI');
  // Failure information messages should be stacked
  const currentErrors = InformationBox.getValue();
  InformationBox.setValue(`${currentErrors}\n${message}`);
  ConnectionForm.focus();
  Screen.render();
});

/**
 * When the Connection Emitter receives a 'success' event, this signifies
 * that the Chat UI should now be shown and the Connection form itself can
 * then be hidden. Any errors shown on the connection screen should also be
 * cleared.
 * @listens ConnectionEmitter.success
 * @emits ConnectionEmitter.hideUI
 * @emits ChatEmitter.showUI
 */
ConnectionEmitter.on('success', () => {
  InformationBox.setValue('');
  ConnectionEmitter.emit('hideUI');
  ChatEmitter.emit('showUI');
});
