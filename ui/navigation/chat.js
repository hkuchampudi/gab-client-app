/* Name: Chat Screen Navigational Logic
 * Description: This file contains the implementation
 * logic for UI components on the chat screen.
 */
// Load NPM modules
const FileSystem = require('fs');

// Load the UI widgets that belong on the chat screen.
const Screen = require('../screen');
const ChatHistoryBox = require('../widgets/chat_screen/box_history');
const OnlineUsersList = require('../widgets/chat_screen/list_users');
const FileTransferRequestList = require('../widgets/chat_screen/list_files');
const FileTransferRequestApprovalQuestion = require('../widgets/chat_screen/question_file');
const HelpfulInformationBox = require('../widgets/chat_screen/box_help');
const MessageInputBox = require('../widgets/chat_screen/input_message');

// Load other necessary components
const FileTransferManager = require('../../classes/fileTransfer.class');
const StyleManager = require('../../classes/styleManager.class');
const ChatEmitter = require('../../modules/emitters/chat.emitter');
const ClientInputParser = require('../../modules/parsers/clientInput.parser');

// Regex
const REGEX_FILE_TRANSFER_LIST_FORMAT = /^(\S*) - (.*): (.*)$/;

// Keyboard bindings for Chat UI
// Exit the application by pressing Ctrl-C
Screen.key(['C-c'], () => process.exit(0));
MessageInputBox.key(['C-c'], () => process.exit(0));

// Escape button toggle between Chat UI components
MessageInputBox.key(['escape'], () => ChatHistoryBox.focus());
ChatHistoryBox.key(['escape'], () => OnlineUsersList.focus());
OnlineUsersList.key(['escape'], () => FileTransferRequestList.focus());
FileTransferRequestList.key(['escape'], () => MessageInputBox.focus());

/**
 * When the 'Enter' key is pressed in the Message Input Box, we
 * want to submit the data in the message to the parser which
 * then constructs the appropriate message to send to the GAB server.
 * After the message is sent to the parser, the input is cleared.
 * @listens MessageInputBox.submit
 */
MessageInputBox.on('submit', () => {
  const input = MessageInputBox.getValue();
  MessageInputBox.clearValue();
  ClientInputParser(input);
  MessageInputBox.focus();
  Screen.render();
});

/**
 * When the d key is pressed while in the OnlineUsersList,
 * a direct message will be composed in the MessageInputBox
 * for the currently selected user
 */
OnlineUsersList.key(['d'], () => {
  // Get the selected user
  const selectedUser = OnlineUsersList
    .getItem(OnlineUsersList.selected)
    .getText();
  MessageInputBox.setValue(`/direct ${selectedUser} `);
  MessageInputBox.focus();
  MessageInputBox.render();
});

/**
 * When the f key is pressed while in the OnlineUsersList,
 * a file send message will be composed in the MessageInputBox
 * for the currently selected user
 */
OnlineUsersList.key(['f'], () => {
  // Get the selected user
  const selectedUser = OnlineUsersList
    .getItem(OnlineUsersList.selected)
    .getText();
  MessageInputBox.setValue(`/file send ${selectedUser} `);
  MessageInputBox.focus();
  MessageInputBox.render();
});

/**
 * Listener for when the Chat UI should be shown
 * @listens ChatEmitter.showUI
 */
ChatEmitter.on('showUI', () => {
  ChatHistoryBox.show();
  OnlineUsersList.show();
  HelpfulInformationBox.show();
  MessageInputBox.show();
  ChatHistoryBox.focus();
  FileTransferRequestList.show();
  Screen.render();
});

/**
 * Listener for when the Chat UI should be hidden
 * @listens ChatEmitter.hideUI
 */
ChatEmitter.on('hideUI', () => {
  MessageInputBox.setValue('');
  ChatHistoryBox.setContent('');
  MessageInputBox.hide();
  ChatHistoryBox.hide();
  OnlineUsersList.hide();
  HelpfulInformationBox.hide();
  FileTransferRequestList.hide();
});

/**
 * Prints a help dialogue for valid GAB client commands
 * @listens ChatEmitter.printHelp
 */
ChatEmitter.on('printHelp', () => {
  const helpMessage = `GABCLIENT:

    GAB Client Commands: ----------------------------------------------------------
    - Command: [message]
      Description: Sends [message] to all online users

    - Command: /direct [username] [message]
      Description: Sends direct message [message] to online user: [username]

    - Command: /whoami
      Description: Asks the server to identify the current user

    - Command: /clear
      Description: Clears the chat history

    - Command: /save
      Description: Saves the chat history to file

    - Command: /file send [username] [filePath]
      Description: Sends a file transfer request to [username]

    - Command: /help
      Description: Displays this help message
    -------------------------------------------------------------------------------
  `;
  const styledMessage = StyleManager.styleMessage(helpMessage);
  ChatHistoryBox.pushLine(styledMessage);
  ChatHistoryBox.setScrollPerc(100);
  Screen.render();
});

/**
 * Clears the chat history
 * @listens ChatEmitter.clearScreen
 */
ChatEmitter.on('clearScreen', () => {
  ChatHistoryBox.setContent('');
  Screen.render();
});

/**
 * When the save command is entered, the contents of the Chat History
 * Box will be saved to file. The file will have the current timestamp
 * in the name and will be saved in the root application directory.
 * @listens ChatEmitter.saveChat
 */
ChatEmitter.on('saveChat', () => {
  const chatHistory = ChatHistoryBox.getContent();
  FileSystem.writeFile(`${new Date().getTime().toString()}_ChatHistory.txt`, chatHistory, () => {
    const message = 'GABCLIENT: Successfully saved chat history';
    const styledMessage = StyleManager.styleMessage(message);
    ChatHistoryBox.pushLine(styledMessage);
    ChatHistoryBox.setScrollPerc(100);
    Screen.render();
  });
});

/**
 * Listens for direct messages to place in the chat history box
 * @listens ChatEmitter.directMessage
 */
ChatEmitter.on('directMessage', (from, to, message) => {
  const formattedMessage = `${from} --> ${to}: ${message}`;
  const styledMessage = StyleManager.styleMessage(formattedMessage);
  ChatHistoryBox.pushLine(styledMessage);
  ChatHistoryBox.setScrollPerc(100);
  Screen.render();
});

/**
 * When an updated userlist is received from the GAB server, the
 * existing list of Online Users should be cleared, and the updated
 * list of Online Users received from the GAB server should be used
 * @listens ChatEmitter.userlist
 */
ChatEmitter.on('userlist', (users) => {
  OnlineUsersList.clearItems();
  OnlineUsersList.setItems(users);
  Screen.render();
});

/**
 * When a chat message is received from the GAB server, the sender
 * and their message is appended to the Chat History
 * @listens ChatEmitter.chatMessage
 */
ChatEmitter.on('chatMessage', (from, message) => {
  const formattedMessage = `${from}: ${message}`;
  const styledMessage = StyleManager.styleMessage(formattedMessage);
  ChatHistoryBox.pushLine(styledMessage);
  ChatHistoryBox.setScrollPerc(100);
  Screen.render();
});

/**
 * A generic message is appended to the Chat History
 * @listens ChatEmitter.genericMessage
 */
ChatEmitter.on('genericMessage', (message) => {
  const formattedMessage = message;
  const styledMessage = StyleManager.styleMessage(formattedMessage);
  ChatHistoryBox.pushLine(styledMessage);
  ChatHistoryBox.setScrollPerc(100);
  Screen.render();
});

/**
 * When a user joined message is received from the GAB server,
 * the joined user will be added to the Chat History Box
 * @listens ChatEmitter.userJoined
 */
ChatEmitter.on('userJoined', (username) => {
  const formattedMessage = `GABSERVER: ${username} has joined the chat`;
  const styledMessage = StyleManager.styleMessage(formattedMessage);
  ChatHistoryBox.pushLine(styledMessage);
  ChatHistoryBox.setScrollPerc(100);
  Screen.render();
});

/**
 * When a user left message is received from the GAB server,
 * the left user will be added to the Chat History Box
 * @listens ChatEmitter.userLeft
 */
ChatEmitter.on('userLeft', (username) => {
  const formattedMessage = `GABSERVER: ${username} has left the chat`;
  const styledMessage = StyleManager.styleMessage(formattedMessage);
  ChatHistoryBox.pushLine(styledMessage);
  ChatHistoryBox.setScrollPerc(100);
  Screen.render();
});

/**
 * When a whoami message is received from the GAB server,
 * the current user's username will be added to the Chat
 * History Box
 * @listens ChatEmitter.whoAmI
 */
ChatEmitter.on('whoAmI', (username) => {
  const formattedMessage = `GABSERVER: You are ${username}`;
  const styledMessage = StyleManager.styleMessage(formattedMessage);
  ChatHistoryBox.pushLine(styledMessage);
  ChatHistoryBox.setScrollPerc(100);
  Screen.render();
});

/**
 * When the GAB Server responds that the specified target for
 * a direct message is not available, this listener is triggered.
 * This listener notifies the current user that the user they
 * wanted to message is not currently online.
 * @listens ChatEmitter.userNotFound
 */
ChatEmitter.on('userNotFound', (username) => {
  const formattedMessage = `GABSERVER: Could not deliver message to ${username}: user not online`;
  const styledMessage = StyleManager.styleMessage(formattedMessage);
  ChatHistoryBox.pushLine(styledMessage);
  ChatHistoryBox.setScrollPerc(100);
  Screen.render();
});

/**
 * When an unknown command is entered by the user, display
 * that the command is unknown, and that they should look
 * at /help if they want to view valid commands.
 * @listens ChatEmitter.unknownCommand
 */
ChatEmitter.on('unknownCommand', (input) => {
  const formattedMessage = `GABCLIENT: Unsure what to do with '${input}' type /help to view valid commands`;
  const styledMessage = StyleManager.styleMessage(formattedMessage);
  ChatHistoryBox.pushLine(styledMessage);
  ChatHistoryBox.setScrollPerc(100);
  Screen.render();
});

/**
 * When a file transfer request has been selected in file transfer list
 * prompt the user if they want to accept or reject the file, and handle
 * the response appropriately.
 * @listens FileTransferRequestList.select
 * @emits ChatEmitter.genericMessage
 */
FileTransferRequestList.on('select', (element) => {
  // Parse the element data
  const selectedFile = element.content;
  const elementComponents = REGEX_FILE_TRANSFER_LIST_FORMAT.exec(selectedFile);
  const uuid = elementComponents[3];
  const fileSearch = FileTransferManager.incomingFileInformation(uuid);
  if (fileSearch.success) {
    const { sender, filename } = fileSearch.message;
    const fileSize = fileSearch.message.file_size;
    FileTransferRequestApprovalQuestion.ask(`





    Do you want to accept (y) or reject (n) the following file:

    - Sender:    ${sender}
    - Filename:  ${filename}
    - File size: ${fileSize} bytes    
    `,
    (_, value) => {
      if (value) {
        FileTransferRequestList.removeItem(element);
        FileTransferManager.acceptRequest(uuid);
        ChatEmitter.emit('genericMessage', `GABCLIENT: <FILE TRANSFER ACCEPTED> ${sender} - ${filename}`);
        Screen.render();
      } else {
        FileTransferRequestList.removeItem(element);
        FileTransferManager.rejectRequest(uuid);
        ChatEmitter.emit('genericMessage', `GABCLIENT: <FILE TRANSFER REJECTED> ${sender} - ${filename}`);
        Screen.render();
      }
    });
  } else {
    FileTransferRequestList.removeItem(element);
  }
});

/**
 * Adds an incoming file transfer request to the list of
 * requests in the UI
 * @listens ChatEmitter.fileTransferRequest
 */
ChatEmitter.on('fileTransferRequest', (from, filename, uuid) => {
  FileTransferRequestList.addItem(`${from} - ${filename}: ${uuid}`);
  Screen.render();
});

/**
 * Signals that the remote user has accepted the file transfer
 * @listens ChatEmitter.fileTransferAccepted
 */
ChatEmitter.on('fileTransferAccepted', (from) => {
  const formattedMessage = `GABCLIENT: <FILE TRANSFER ACCEPTED> by ${from} - Sending...`;
  const styledMessage = StyleManager.styleMessage(formattedMessage);
  ChatHistoryBox.pushLine(styledMessage);
  ChatHistoryBox.setScrollPerc(100);
  Screen.render();
});

/**
 * Signals that the remote user has rejected the file transfer
 * @listens ChatEmitter.fileTransferRejected
 */
ChatEmitter.on('fileTransferRejected', (from) => {
  const formattedMessage = `GABCLIENT: <FILE TRANSFER REJECTED> by ${from}`;
  const styledMessage = StyleManager.styleMessage(formattedMessage);
  ChatHistoryBox.pushLine(styledMessage);
  ChatHistoryBox.setScrollPerc(100);
  Screen.render();
});

/**
 * Signals that the remote user has started the file transfer
 * @listens ChatEmitter.fileTransferStarted
 */
ChatEmitter.on('fileTransferStarted', (from) => {
  const formattedMessage = `GABCLIENT: <FILE TRANSFER STARTED> by ${from}`;
  const styledMessage = StyleManager.styleMessage(formattedMessage);
  ChatHistoryBox.pushLine(styledMessage);
  ChatHistoryBox.setScrollPerc(100);
  Screen.render();
});

/**
 * Signals that the remote user has finished the file transfer
 * @listens ChatEmitter.fileTransferCompleted
 */
ChatEmitter.on('fileTransferCompleted', (from) => {
  const formattedMessage = `GABCLIENT: <FILE TRANSFER FINISHED> from ${from}`;
  const styledMessage = StyleManager.styleMessage(formattedMessage);
  ChatHistoryBox.pushLine(styledMessage);
  ChatHistoryBox.setScrollPerc(100);
  Screen.render();
});

/**
 * Signals that an error occurred during the file transfer
 * @listens ChatEmitter.fileTransferError
 */
ChatEmitter.on('fileTransferError', (from) => {
  const formattedMessage = `GABCLIENT: <FILE TRANSFER ERROR> from ${from}`;
  const styledMessage = StyleManager.styleMessage(formattedMessage);
  ChatHistoryBox.pushLine(styledMessage);
  ChatHistoryBox.setScrollPerc(100);
  Screen.render();
});
