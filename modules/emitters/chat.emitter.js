/* Name: Chat Screen Event Emitter
 * Description: Handles events related to the Chat UI
 * in the GAB client application's chat screen. The
 * following are events being listened for:
 *
 * ChatEmitter Events
 * - emit('showUI')
 * - emit('hideUI')
 * - emit('userlist', userlist)
 * - emit('chatMessage', from, message)
 * - emit('directMessage', from, to, message)
 * - emit('whoAmI', username)
 * - emit('userJoined', username)
 * - emit('userLeft', username)
 * - emit('userNotFound', username)
 * - emit('clearScreen')
 * - emit('printHelp')
 * - emit('saveChat')
 * - emit('unknownCommand')
 * - emit('genericMessage', message)
 * - emit('fileTransferRequest', from, filename, uuid)
 * - emit('fileTransferAccepted', from, uuid)
 * - emit('fileTransferRejected', from, uuid)
 * - emit('fileTransferStarted', from, uuid)
 * - emit('fileTransferCompleted', from, uuid)
 * - emit('fileTransferError', from, uuid)
 * - emit('fileChunkReceived', from, uuid, progress)
 */
// Load required modules
const EventEmitter = require('events');

// Create an Event Emitter instance
const ChatEmitter = new EventEmitter.EventEmitter();

// Export the created event emitter
module.exports = ChatEmitter;
