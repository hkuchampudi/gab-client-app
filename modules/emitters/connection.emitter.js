/* Name: Connection Form Event Emitter
 * Description: Handles events related to the Connection Form
 * in the GAB client application's connection screen. The following
 * are events being listened for:
 *
 * ConnectionEmitter Events
 * - emit('success')
 * - emit('failure', error)
 * - emit('showUI')
 * - emit('hideUI')
 */
// Load required modules
const EventEmitter = require('events');

// Create an Event Emitter instance
const ConnectionEmitter = new EventEmitter.EventEmitter();

// Export the created event emitter
module.exports = ConnectionEmitter;
