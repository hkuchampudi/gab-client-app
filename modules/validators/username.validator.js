// Load required NPM modules
const Assert = require('chai').assert;

// Define regular expressions
const REGEX_USERNAME = /^[a-z0-9]{3,10}$/i;

/**
 * Determines if the provided username is valid.
 * @param {string} username - Username to validate
 * @returns {object} JSON object containing 'success' (boolean) and 'message' (string)
 */
function ValidateUsername(username) {
  try {
    // Ensure that the username exists, is a string,
    // is at least 3 characters, is at most 10 characters,
    // and is in a valid format
    Assert.exists(username, 'username must be provided');
    Assert.typeOf(username, 'string', 'username must be of type string');
    Assert.isAtLeast(username.length, 3, 'username must have at least 3 characters');
    Assert.isAtMost(username.length, 10, 'username must have at most 10 characters');
    Assert.match(username, REGEX_USERNAME, 'username is not valid');
    // Return success message if all assertions pass
    return {
      success: true,
      message: username,
    };
  } catch (error) {
    // Return an error message if an assertion fails
    return {
      success: false,
      message: error.message,
    };
  }
}

// Export the created validator module
module.exports = ValidateUsername;
