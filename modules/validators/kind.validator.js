// Load required NPM modules
const Assert = require('chai').assert;

/**
 * Determines if the provided kind is valid.
 * @param {string} kind - Kind to validate
 * @returns {object} JSON object containing 'success' (boolean) and 'message' (string)
 */
function ValidateKind(kind) {
  try {
    // Ensure that the kind input exists, is a string,
    // and is one of the predefined values
    const validKinds = ['error', 'userlist', 'connection', 'whoami', 'chat', 'direct'];
    Assert.exists(kind, 'kind must be provided');
    Assert.typeOf(kind, 'string', 'kind must be of type string');
    Assert.include(validKinds, kind, 'kind value is invalid');
    // Return success message if all assertions pass
    return {
      success: true,
      message: kind,
    };
  } catch (error) {
    // Return an error message if an assertion fails
    return {
      success: false,
      message: error.message,
    };
  }
}

// Export the created validator module
module.exports = ValidateKind;
