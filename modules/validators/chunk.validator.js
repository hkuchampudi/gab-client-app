// Load required NPM modules
const Assert = require('chai').assert;

/**
 * Determines if a chunk from the GAB Server is formatted correctly
 * @param {object} chunk - Chunk data payload sent by the GAB Server
 * @returns {object} JSON object containing 'success' (boolean) and 'message' (string)
 */
function ValidateChunk(chunk) {
  try {
    // Assert that GAB Server messages containing chunk
    // data in the message data, are structured properly.
    // This means that the data object should contain the
    // following keys: uuid, filename, chunk
    Assert.exists(chunk, 'chunk data object should exist');
    Assert.typeOf(chunk, 'object', 'chunk data should be object');
    Assert.exists(chunk.uuid, 'chunk data payloads should contain a uuid');
    Assert.exists(chunk.filename, 'chunk data payloads should contain a filename');
    Assert.exists(chunk.chunk, 'chunk data payloads should contain a chunk');
    // Return success message if all assertions pass
    return {
      success: true,
      message: chunk,
    };
  } catch (error) {
    // Return an error message if an assertion fails
    return {
      success: false,
      message: error.message,
    };
  }
}

// Export the created validator module
module.exports = ValidateChunk;
