// Load required NPM modules
const Assert = require('chai').assert;

// Load local file dependencies
const ValidateKind = require('./kind.validator');
const ValidateUsername = require('./username.validator');

/**
 * Determines if a message from the GAB Server is formatted correctly
 * @param {object} message - Message sent by the GAB Server
 * @returns {object} JSON object containing 'success' (boolean) and 'message' (string)
 */
function ValidateMessage(message) {
  try {
    // Parse the message into JSON format and ensure
    // that it has the fields: from, to, kind, and data.
    // Additionally, ensure that the from and to fields
    // contain valid usernames and the kind field is valid.
    const parsedJSON = JSON.parse(message);
    // eslint-disable-next-line object-curly-newline
    const { from, to, kind, data } = parsedJSON;
    Assert.exists(from, 'from field must be included in the message object');
    Assert.exists(to, 'to field must be included in the message object');
    Assert.exists(kind, 'kind field must be included in the message object');
    Assert.exists(data, 'data field must be included in the message object');
    const fromUsernameValidation = ValidateUsername(from);
    if (!fromUsernameValidation.success) throw new Error(fromUsernameValidation.message);
    const toUsernameValidation = ValidateUsername(to);
    if (!toUsernameValidation.success) throw new Error(toUsernameValidation.message);
    const kindKindValidation = ValidateKind(kind);
    if (!kindKindValidation.success) throw new Error(kindKindValidation.message);
    // Return success message if all assertions pass
    return {
      success: true,
      message: parsedJSON,
    };
  } catch (error) {
    // Return an error message if an assertion fails
    return {
      success: false,
      message: error.message,
    };
  }
}

// Export the created validator module
module.exports = ValidateMessage;
