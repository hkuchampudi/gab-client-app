// Load required NPM modules
const Assert = require('chai').assert;

// Define regular expressions
const REGEX_HOSTNAME = /^[a-z0-9.-]+$/i;

/**
 * Determines if the provided hostname is valid.
 * @param {string} hostname - Hostname to validate
 * @returns {object} JSON object containing 'success' (boolean) and 'message' (string)
 */
function ValidateHostname(hostname) {
  try {
    // Ensure that the hostname exists, is of type
    // string, and in a valid hostname format
    Assert.exists(hostname, 'hostname must be provided');
    Assert.typeOf(hostname, 'string', 'hostname must be of type string');
    Assert.match(hostname, REGEX_HOSTNAME, 'hostname is not valid');
    // Return success message if all assertions pass
    return {
      success: true,
      message: hostname,
    };
  } catch (error) {
    // Return an error message if an assertion fails
    return {
      success: false,
      message: error.message,
    };
  }
}

// Export the created validator module
module.exports = ValidateHostname;
