// Load required NPM modules
const Assert = require('chai').assert;

/**
 * Determines if the provided port is valid.
 * @param {number} port - Port to validate
 * @returns {object} JSON object containing 'success' (boolean) and 'message' (string)
 */
function ValidatePort(port) {
  try {
    // Ensure that the port input exists, is a number,
    // is at least 0, and is at most 65535
    Assert.exists(port, 'port must be provided');
    Assert.typeOf(port, 'number', 'port must be a number');
    Assert.isAtLeast(port, 0, 'port number cannot be less than 0');
    Assert.isAtMost(port, 65535, 'port number can be at most 65535');
    // Return success message if all assertions pass
    return {
      success: true,
      message: port,
    };
  } catch (error) {
    // Return an error message if an assertion fails
    return {
      success: false,
      message: error.message,
    };
  }
}

// Export the created validator module
module.exports = ValidatePort;
