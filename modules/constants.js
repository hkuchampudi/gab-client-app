module.exports = {
  // Application Name
  APPLICATION_NAME: 'GAB Client',

  // UI Connection Screen Defaults
  UI_HOSTNAME_DEFAULT_VALUE: '127.0.0.1',
  UI_PORT_DEFAULT_VALUE: '4930',
  UI_USERNAME_DEFAULT_VALUE: 'Harsha',

  // Winston Logging File Settings
  WINSTON_LOG_LEVEL: 'debug',
  WINSTON_ERROR_LOG_FILENAME: 'gab-client.error.log',
  WINSTON_ALL_LOG_FILENAME: 'gab-client.all.log',
};
