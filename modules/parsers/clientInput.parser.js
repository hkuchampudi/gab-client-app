// Load required NPM modules

// Load local file dependencies
const ChatEmitter = require('../emitters/chat.emitter');
const FileTransferManager = require('../../classes/fileTransfer.class');
const GABConnection = require('../../classes/connection.class');

// Define regular expressions
const REGEX_OPERATION_CLEAR = /^\/clear(\s*)$/m;
const REGEX_OPERATION_HELP = /^\/help(\s*)$/m;
const REGEX_OPERATION_SAVE_CHAT = /^\/save(\s*)$/m;
const REGEX_OPERATION_DIRECT = /^\/direct (\S*) (.*)$/m;
const REGEX_OPERATION_FILE_SEND = /^\/file send (\S*) (.*)$/m;
const REGEX_OPERATION_WHOAMI = /^\/whoami(\s*)$/m;
const REGEX_OPERATION_UNKNOWN = /^\/.*$/m;

/**
 * Parses the command entered by the user and determines the
 * appropriate action to take.
 *
 * @emits ChatEmitter.clearScreen
 * @emits ChatEmitter.printHelp
 * @emits ChatEmitter.saveChat
 * @emits ChatEmitter.unknownCommand
 * @emits ChatEmitter.directMessage
 * @emits ChatEmitter.genericMessage
 *
 * @param {string} input - Users input into the Chat UI
 */
function ClientInputParser(input) {
  // If the clear operation is entered by the user then
  // the current chat history should be cleared
  if (REGEX_OPERATION_CLEAR.test(input)) {
    ChatEmitter.emit('clearScreen');
  } else if (REGEX_OPERATION_HELP.test(input)) {
    // If the help command is entered, then general help
    // information should be displayed in the Chat History box
    ChatEmitter.emit('printHelp');
  } else if (REGEX_OPERATION_SAVE_CHAT.test(input)) {
    // When the save command is entered, the current contents
    // of the Chat History box should be saved to file
    ChatEmitter.emit('saveChat');
  } else if (REGEX_OPERATION_DIRECT.test(input)) {
    // When the direct command prefix is used, a direct
    // message should be constructed based on the command parameters
    const inputComponents = REGEX_OPERATION_DIRECT.exec(input);
    const username = inputComponents[1];
    const message = inputComponents[2];
    GABConnection.sendDirectMessage(username, message);
    ChatEmitter.emit('directMessage', 'YOU', username, message);
  } else if (REGEX_OPERATION_WHOAMI.test(input)) {
    // If the whoami command is entered, then a whoami message
    // is constructed and then sent to the server
    GABConnection.sendWhoAmI();
  } else if (REGEX_OPERATION_FILE_SEND.test(input)) {
    // When the file command is used with the send parameter,
    // the file specified should be sent to the specified user.
    const inputComponents = REGEX_OPERATION_FILE_SEND.exec(input);
    const username = inputComponents[1];
    const filePath = inputComponents[2];
    const result = FileTransferManager.addOutgoingRequest(username, filePath);
    if (result.success) {
      const formattedMessage = `GABCLIENT: <FILE TRANSFER REQUEST SENT> to ${username}`;
      ChatEmitter.emit('genericMessage', formattedMessage);
    } else {
      const errorMessage = result.message;
      const formattedMessage = `GABCLIENT: <FILE TRANSFER ERROR> to ${username}: ${errorMessage}`;
      ChatEmitter.emit('genericMessage', formattedMessage);
    }
  } else if (REGEX_OPERATION_UNKNOWN.test(input)) {
    // If the command prefix character is used, but does not
    // match any of the commands specified above, then it is
    // an unsupported command
    ChatEmitter.emit('unknownCommand', input);
  } else {
    // In all other cases, a generic chat message destined
    // for all users should be constructed
    GABConnection.sendChatMessage(input);
  }
}

// Export the parser module
module.exports = ClientInputParser;
