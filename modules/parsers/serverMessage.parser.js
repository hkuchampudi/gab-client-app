/* eslint-disable no-underscore-dangle */
// Load local file dependencies
const ConnectionEmitter = require('../emitters/connection.emitter');
const ChatEmitter = require('../emitters/chat.emitter');
const Logger = require('../winston.logger');
const ChunkValidator = require('../validators/chunk.validator');
const MessageValidator = require('../validators/message.validator');
const FileTransferManager = require('../../classes/fileTransfer.class');

// Define regular expressions
const REGEX_FILE_TRANSFER_REQUEST = /^FileTransferRequest::(.+)::(\d+)::([a-zA-Z0-9]{64})$/;
const REGEX_FILE_TRANSFER_ACCEPTED = /^FileTransferAccepted::([a-zA-Z0-9]{64})$/;
const REGEX_FILE_TRANSFER_REJECTED = /^FileTransferRejected::([a-zA-Z0-9]{64})$/;
const REGEX_FILE_TRANSFER_STARTED = /^FileTransferStarted::([a-zA-Z0-9]{64})$/;
const REGEX_FILE_TRANSFER_FINISHED = /^FileTransferFinished::([a-zA-Z0-9]{64})$/;
const REGEX_FILE_TRANSFER_CHUNK_RECEIVED = /^FileTransferChunkReceived::([a-zA-Z0-9]{64})$/;
const REGEX_FILE_TRANSFER_ERROR = /^FileTransferError::([a-zA-Z0-9]{64})$/;
const REGEX_SERVER_MESSAGE_INVALID_USERNAME = /^Your username is invalid/m;
const REGEX_SERVER_MESSAGE_USERNAME_TAKEN = /^Your username is taken/m;
const REGEX_SERVER_MESSAGE_READ_ERROR = /^Error reading message/m;
const REGEX_SERVER_MESSAGE_KIND_ERROR = /^Unknown or invalid kind/m;
const REGEX_SERVER_MESSAGE_NO_PONG = /^Did not get a return Pong/m;
const REGEX_SERVER_MESSAGE_JOIN_SERVER = /^([a-zA-Z0-9]{3,10}) has joined the server!$/m;
const REGEX_SERVER_MESSAGE_LEFT_SERVER = /([a-zA-Z0-9]{3,10}) has left the server!$/m;
const REGEX_SERVER_MESSAGE_WHO_AM_I = /^You are ([a-zA-Z0-9]{3,10})$/m;
const REGEX_SERVER_MESSAGE_USER_NOT_FOUND = /^No user found by the name of (.*?)(\.?)$/m;

/**
 * Parses the server message that is passed in, and then
 * passes the parsed results to the appropriate handler functions
 *
 * @emits ChatEmitter.userlist
 * @emits ChatEmitter.chatMessage
 * @emits ChatEmitter.directMessage
 * @emits ChatEmitter.userJoined
 * @emits ChatEmitter.userLeft
 * @emits ChatEmitter.userNotFound
 * @emits ChatEmitter.whoAmI
 * @emits ChatEmitter.fileChunkReceived
 * @emits ChatEmitter.fileTransferRequest
 * @emits ChatEmitter.fileTransferAccepted
 * @emits ChatEmitter.fileTransferRejected
 * @emits ChatEmitter.fileTransferStarted
 * @emits ChatEmitter.fileTransferCompleted
 * @emits ChatEmitter.fileTransferError
 * @emits ConnectionEmitter.failure
 *
 * @param {object} message - GAB message received from the GAB Server
 */
function ServerMessageParser(message) {
  // Validate the incoming message and, if it is
  // not valid, log the error
  const validMessage = MessageValidator(message);
  if (!validMessage.success) {
    Logger.warn(`invalid message received from the server: ${message}`);
  } else {
    // Otherwise, if the server message was successfully
    // validated, the appropriate action should be taken
    // depending on the message contents
    const parsedJSON = validMessage.message;
    // eslint-disable-next-line object-curly-newline
    const { from, to, kind, data } = parsedJSON;
    // If a userlist message is received from the server,
    // the message also contains a list of online users.
    // This list should be parsed into an array and passed
    // to the appropriate handler
    if (kind === 'userlist') {
      let usersArray = data.split(',');
      usersArray = usersArray.filter(user => user !== to);
      ChatEmitter.emit('userlist', usersArray);
    } else if (kind === 'chat') {
      // If a chat message is received from the server,
      // ensure that the data payload is of type string
      // before passing the chat message to a handler
      if (typeof data === 'string') ChatEmitter.emit('chatMessage', from, data);
      else Logger.info(`received unidentified data in chat message: ${data}`);
    } else if (kind === 'direct') {
      // If a direct message is received from the server,
      // check the type of the data payload. If it is an
      // object, then it should be a file chunk that should
      // be parsed and handled.
      if (typeof data === 'object' && data !== null) {
        const validChunk = ChunkValidator(data);
        if (!validChunk.success) Logger.info(`invalid direct message payload received: ${data}`);
        else {
          const { uuid, chunk } = validChunk.message;
          FileTransferManager.receiveFileChunk(uuid, chunk);
          ChatEmitter.emit('fileChunkReceived', from, uuid);
        }
      } else if (typeof data === 'string') {
        // If a direct message is received from a user and the
        // data payload is of type string, check if a file transfer
        // operation is required. Otherwise, the direct message can
        // be passed directly.
        if (REGEX_FILE_TRANSFER_REQUEST.test(data)) {
          // If a file transfer request is received, file information
          // should be extracted and added to the client's incoming
          // file requests
          const requestComponents = REGEX_FILE_TRANSFER_REQUEST.exec(data);
          const filename = requestComponents[1];
          const fileSize = requestComponents[2];
          const uuid = requestComponents[3];
          FileTransferManager.addIncomingRequest(from, filename, fileSize, uuid, (result) => {
            if (result.success) {
              const parsedFrom = result.message.sender;
              const parsedFilename = result.message.filename;
              ChatEmitter.emit('fileTransferRequest', parsedFrom, parsedFilename, uuid);
            }
          });
        } else if (REGEX_FILE_TRANSFER_ACCEPTED.test(data)) {
          // The remote user has accepted the file transfer request.
          // Therefore, we can send the user the file
          const transferComponents = REGEX_FILE_TRANSFER_ACCEPTED.exec(data);
          const uuid = transferComponents[1];
          FileTransferManager.sendFile(uuid, (error) => {
            if (!error) {
              FileTransferManager.sendNextChunk(uuid, (status, information) => {
                const { filename, recipient, value } = information;
                if (status === 'completed') ChatEmitter.emit('genericMessage', `GABCLIENT: <FILE TRANSFER COMPLETED> ${filename} --> ${recipient}`);
                else if (status === 'error') {
                  const formattedMessage = `GABCLIENT: <FILE TRANSFER ERROR> to ${recipient}: ${value}`;
                  ChatEmitter.emit('genericMessage', formattedMessage);
                }
              });
            }
          });
          ChatEmitter.emit('fileTransferAccepted', from, uuid);
        } else if (REGEX_FILE_TRANSFER_REJECTED.test(data)) {
          // The remote user has rejected the file transfer request.
          // Therefore, we must delete the local outgoingRequest
          const transferComponents = REGEX_FILE_TRANSFER_REJECTED.exec(data);
          const uuid = transferComponents[1];
          FileTransferManager.__removeOutgoingRequest(uuid);
          ChatEmitter.emit('fileTransferRejected', from, uuid);
        } else if (REGEX_FILE_TRANSFER_STARTED.test(data)) {
          // If the remote user has started the file transfer process,
          // an event should be emitted so interested parties can take action
          const transferComponents = REGEX_FILE_TRANSFER_STARTED.exec(data);
          const uuid = transferComponents[1];
          ChatEmitter.emit('fileTransferStarted', from, uuid);
        } else if (REGEX_FILE_TRANSFER_FINISHED.test(data)) {
          // If the remote user signals that they have finished transferring
          // the file then it can be removed from the local incomingRequests
          const transferComponents = REGEX_FILE_TRANSFER_FINISHED.exec(data);
          const uuid = transferComponents[1];
          FileTransferManager.__removeIncomingRequest(uuid);
          ChatEmitter.emit('fileTransferCompleted', from, uuid);
        } else if (REGEX_FILE_TRANSFER_CHUNK_RECEIVED.test(data)) {
          // If the remote user successfully receives a chunk that was sent to
          // them, send them the next chunk. If all chunks have been sent, tell
          // them that the file transfer has completed
          const transferComponents = REGEX_FILE_TRANSFER_CHUNK_RECEIVED.exec(data);
          const uuid = transferComponents[1];
          FileTransferManager.sendNextChunk(uuid, (status, information) => {
            const { filename, recipient, value } = information;
            if (status === 'completed') ChatEmitter.emit('genericMessage', `GABCLIENT: <FILE TRANSFER COMPLETED> ${filename} --> ${recipient}`);
            else if (status === 'error') {
              const formattedMessage = `GABCLIENT: <FILE TRANSFER ERROR> to ${recipient}: ${value}`;
              ChatEmitter.emit('genericMessage', formattedMessage);
            }
          });
        } else if (REGEX_FILE_TRANSFER_ERROR.test(data)) {
          // If the remote user signals that a file transfer error
          // has occurred, any data sent already should be deleted,
          // the incomingRequest entry should be removed, and an
          // event should be emitted
          const transferComponents = REGEX_FILE_TRANSFER_ERROR.exec(data);
          const uuid = transferComponents[1];
          FileTransferManager.__removeIncomingRequest(uuid);
          ChatEmitter.emit('fileTransferError', from, uuid);
        } else {
          // For all other direct messages, no other special
          // action needs to be taken. The data can be passed into
          // the emitter directly
          ChatEmitter.emit('directMessage', from, 'YOU', data);
        }
      } else {
        // Some invalid data sent by a remote client. Just log it for
        // later analysis
        Logger.info(`received unidentified data in direct message: ${data}`);
      }
    } else if (REGEX_SERVER_MESSAGE_INVALID_USERNAME.test(data)) {
      // If the server responds that an invalid username was
      // provided to connect to the GAB Server, the user should
      // be kicked back to the connection screen, and an error
      // message shown
      ConnectionEmitter.emit('failure', data);
    } else if (REGEX_SERVER_MESSAGE_USERNAME_TAKEN.test(data)) {
      // If the server responds that the selected username has
      // already been taken by another user, the user should be
      // kicked back to the connection screen, and an error message shown
      ConnectionEmitter.emit('failure', data);
    } else if (REGEX_SERVER_MESSAGE_READ_ERROR.test(data)) {
      // If the GAB Server was unable to understand the message
      // sent to it, it sends this message as a response. No action
      // is necessary; however, the event should be logged
      Logger.warn(data);
    } else if (REGEX_SERVER_MESSAGE_KIND_ERROR.test(data)) {
      // If the GAB Server does not recognize the kind of message
      // sent to it, it sends this response. No action is necessary;
      // however, the event should be logged
      Logger.warn(data);
    } else if (REGEX_SERVER_MESSAGE_NO_PONG.test(data)) {
      // If the GAB Server does not receive a PONG from the
      // GAB Client when it sends a PING, then it will
      // terminate the connection. When this occurs, the user
      // should be kicked back to the connection screen
      ConnectionEmitter.emit('failure', data);
    } else if (REGEX_SERVER_MESSAGE_JOIN_SERVER.test(data)) {
      // When a new user joins the server, the username of the
      // joined user should be passed on. Additionally, the
      // list of online users should also be updated by sending
      // a userlist message to the GAB Server
      const username = REGEX_SERVER_MESSAGE_JOIN_SERVER.exec(data)[1];
      ChatEmitter.emit('userJoined', username);
      this.updateUserlist();
    } else if (REGEX_SERVER_MESSAGE_LEFT_SERVER.test(data)) {
      // When a user leaves the server, the username of the user
      // should be passed-on. Additionally, the list of online
      // users should also be updated by sending a userlist message
      // to the GAB Server
      const username = REGEX_SERVER_MESSAGE_LEFT_SERVER.exec(data)[1];
      ChatEmitter.emit('userLeft', username);
      this.updateUserlist();
    } else if (REGEX_SERVER_MESSAGE_WHO_AM_I.test(data)) {
      // If a whoami message is received from the server, the username
      // should be parsed out and passed-on
      const username = REGEX_SERVER_MESSAGE_WHO_AM_I.exec(data)[1];
      ChatEmitter.emit('whoAmI', username);
    } else if (REGEX_SERVER_MESSAGE_USER_NOT_FOUND.test(data)) {
      // If the GAB Server could not find the destination user (i.e.
      // the user is not online) it sends this message back to the
      // sending user
      const username = REGEX_SERVER_MESSAGE_USER_NOT_FOUND.exec(data)[1];
      ChatEmitter.emit('userNotFound', username);
    } else {
      // Handle all other messages that have not yet been detected
      Logger.info(`unsure how to handle message: ${message}`);
    }
  }
}

// Export the parser function
module.exports = ServerMessageParser;
