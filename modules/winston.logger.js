/* Name: Winston Logger
 * Description: Module exports the configuration for the
 * Winston logger. The logger configuration defines how to
 * display and store messages logged.
 */
const Winston = require('winston');
const Constants = require('./constants');

// Error logging configuration
const LoggerErrorLog = new Winston.transports.File({
  filename: Constants.WINSTON_ERROR_LOG_FILENAME,
  level: 'error',
});

// All logs configuration
const LoggerAllLog = new Winston.transports.File({
  filename: Constants.WINSTON_ALL_LOG_FILENAME,
  level: 'debug',
});

// Logging to system console configuration
// const LoggerConsoleLog = new Winston.transports.Console({
//   format: Winston.format.simple(),
// });

// Create the Winston logger to export
const Logger = Winston.createLogger({
  level: Constants.WINSTON_LOG_LEVEL,
  format: Winston.format.json(),
  transports: [
    LoggerErrorLog,
    LoggerAllLog,
  ],
});

// If the application is not running in a production env,
// then the application should log to the console as well.
// if (process.env.NODE_ENV !== 'production') Logger.add(LoggerConsoleLog);

// Export the configured logger
module.exports = Logger;
