# Gift of Asynchronous Babble (GAB) Client

[![asciicast](https://asciinema.org/a/alN0CkoRJHW1ruiEY1TCnHvyg.svg)](https://asciinema.org/a/alN0CkoRJHW1ruiEY1TCnHvyg)

## Table of Contents
- [Gift of Asynchronous Babble (GAB) Client](#gift-of-asynchronous-babble--gab--client)
  * [Table of Contents](#table-of-contents)
  * [Introduction](#introduction)
  * [Usage Instructions](#usage-instructions)
  * [Connection Screen](#connection-screen)
  * [Chat Screen](#chat-screen)
    + [Sending Chat Messages](#sending-chat-messages)
    + [Sending Direct Messages](#sending-direct-messages)
    + [Identifying the Current User](#identifying-the-current-user)
    + [Transferring Files](#transferring-files)
      - [Sending a File Transfer Request](#sending-a-file-transfer-request)
      - [Approving/Rejecting Incoming File Transfer Requests](#approving-rejecting-incoming-file-transfer-requests)
    + [Saving Chat History](#saving-chat-history)
    + [Theme Messages](#theme-messages)
  * [Logging](#logging)
  * [Architecture Notes](#architecture-notes)
    + [File Transfer Protocol](#file-transfer-protocol)
  * [Resources](#resources)

## Introduction
This repository contains the source code for a GAB client that interfaces with a compliant [GAB Server](https://github.com/hkuchampudi/gab-server). The following sections will provide additional information on how to use the application, as well as client implementation details.

## Usage Instructions
Before running the GAB client application, `npm install` must be run in the folder to install all of the necessary dependencies for running the client. After installing necessary dependencies, the application can be started by running:

```
npm start
```

## Connection Screen
After running `npm start`, the user is presented with a login screen containing form input fields for: **HOSTNAME/IP**, **PORT**, and **USERNAME**. The user can use the `[Tab]` key to cycle between form elements, or press `[Ctrl-c]` to exit the application.

- The **HOSTNAME/IP** field should contain the hostname or the IP address where the GAB server can be reached. By default, it is assumed that the GAB server is running locally; therefore, `127.0.0.1` is used.

- The **PORT** field contains the port that the GAB server is listening for WebSocket connections on. By default, the GAB server is run on port `4930`; therefore, it is the default on the GAB client.

- The **USERNAME** field contains the username that the user will be identified by on the GAB server. By default, the username `Harsha` is set.

To change the default values for the connection screen, the relevant keys in `modules/constants.js` can be modified.

## Chat Screen
After successfully, connecting to the GAB server, the connection screen will be replaced by the chat screen. To cycle between different panes in the chat screen, the `[Esc]` key is used. At any time, the `[Ctrl-c]` key combination can be used to exit the application.  

  ### Sending Chat Messages
  In order to send a chat message to all online members of the GAB server, cycle between the panes using the `[Esc]` key until the message compose box is at the bottom of the screen is selected. Once focused, the user can input any text that they would like sent to all online users connected to the GAB server. Once the desired message is composed, pressing the `[Enter]` key while focused on the message compose box will send a message .

  ### Sending Direct Messages
  In order to send a direct message, cycle between the panes using the `[Esc]` key until the message compose box at the bottom of the screen is selected. Once focused, in order to send a direct message, the user must format their message like so:
  ```
  /direct [user to send message to] [message]
  ```
  Once, the message is composed, the `[Enter]` key can be pressed to send the direct message to the specified user.

  Alternatively, as a shortcut, the user can cycle panes using the `[Esc]` key until they are focused on the **Online Users** pane. Then, using the `[Up]` and `[Down]` arrow keys, they can scroll to the user they want to send the direct message to. Once the desired user is highlighted, pressing the `[d]` key will automatically template a direct message to the selected user.

  ### Identifying the Current User
  In order to identify the current user, cycle between the panes using the `[Esc]` key until the message compose box at the bottom of the screen is selected. Once focused, the user should enter the following command:
  ```
  /whoami
  ```
  The GAB server will then respond to the user and identify who they are (as seen by the GAB server and other online users).

  ### Transferring Files
  **Important Note: Please ensure that the `files/` directory exists in the same directory as where the GAB client was run from; this is where transferred files will be written**

  #### Sending a File Transfer Request
  This GAB client application also has the ability to transfer files to other users using this client software and connected to the GAB server. To send a file transfer request to another user, cycle between the panes using the `[Esc]` key until the message compose box at the bottom of the screen is selected. Once focused, the user should enter the following command:
  ```
  /file send [user to send request to] [system path of the file to send]
  ```
  Pressing the `[Enter]` key will send the file transfer request to the specified user. As a shortcut, like with direct messages, the user can cycle panes until they are focused on the **Online Users** pane. Then, using the `[Up]` and `[Down]` arrow keys, they can scroll to the user they want to send a file to. Once the desired user is highlighted, pressing the `[f]` key will automatically template a file transfer message to the selected user.

  #### Approving/Rejecting Incoming File Transfer Requests
  When a user receives a file transfer request from another user, the request will appear in the **File Transfer Requests** pane. To approve or reject a file transfer request from another user, cycle between the panes using the `[Esc]` key until the **File Transfer Requests** pane is focused. Next, use the `[Up]` and `[Down]` arrow keys to navigate to the file transfer request to approve or reject. Press the `[Enter]` key to select the highlighted request. A prompt will be displayed to the user asking whether they want to approve (`[y]`) or reject (`[n]`) the file transfer request. If the file transfer request is approved, the sending GAB client will begin the transmission immediately. Both the sender and recipient will be notified about the status of the file transfer throughout the transfers life-cycle. Received files can be found in the `files/` directory.

  ### Saving Chat History
  At any time, the user has the ability to save the current contents of the **Chat History**. In order to do this, the user should cycle between panes using the `[Esc]` key until the message compose box at the bottom of the screen is selected. Then, the following command can be entered to save the contents of **Chat History**:
  ```
  /save
  ```
  This will create a chat history file in the directory where the GAB client was run from. The file will be named in the following manner: 
  ```
  [unix timestamp]_ChatHistory.txt
  ```

  ### Theme Messages
  This GAB client supports the theming of messages. Message themes are determined by the contents of `style.json` in the root of the application directory. The contents of `style.json` must be an array containing JSON objects. Each JSON object entry must contain the following keys:
  - **pattern**: The `RegExp` tested on a string to determine if the style applies to the string. Currently, custom user `RegExp` options are not supported; the **pattern** will be applied globally to a string. Additionally, the `/  /` should be omitted from the pattern.
  - **type**: The **type** can only be one of two options:
    - **color**: When the user wants to change the color of matched elements in a string.
    - **attribute**: When the user wants to change the presentation of matched elements in a string.
  - **value**: The value of the modification.
    - For entries with type **color**, the **value** should be a valid, six character hex color code.
    - For entries with type **attribute**, the **value** should be one of the following values based on the desired effect:
      - **bold**: To make matched elements bolded.
      - **underline**: To make matched elements underlined.
      - **blink**: To make matched elements blink.
      - **inverse**: To invert the matched elements.
      - **invisible**: To make matched elements invisible.

  In the event that a `style.json` is not-present, or there is an error in the configuration, an error will be logged into the log file, and an no styles will be applied. After making changes to the `style.json`, the GAB client must be restarted for those changes to take effect.

## Logging
By default, two log files are created:
- `gab-client.all.log` contains all messages logged.
- `gab-client.error.log` contains only error messages logged.

To change the default values for the log filenames, the relevant keys in `modules/constants.js` can be modified.

## Architecture Notes

### File Transfer Protocol
**NOTE: when Harsha sends file chunks to Ben, Ben will acknowledge every chunk received. Only after a chunk is acknowledged will Harsha send 
the next chunk.** This is a useful technique for alleviating backpressure from the sender.

![File Transfer Overview](images/transfer_protocol.png)

## Resources
- [BlessedJS](https://github.com/chjj/blessed)
- [ws](https://www.npmjs.com/package/ws)
